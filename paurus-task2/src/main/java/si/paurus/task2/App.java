package si.paurus.task2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.microsoft.sqlserver.jdbc.SQLServerBulkCSVFileRecord;
import com.microsoft.sqlserver.jdbc.SQLServerBulkCopy;
import com.microsoft.sqlserver.jdbc.SQLServerBulkCopyOptions;
import com.microsoft.sqlserver.jdbc.SQLServerException;

public class App {

	/** DB table name */
	private static final String TABLE_NAME = "PerformanceTable";

	/** batch size */
	private static final int BATCH_SIZE = 150_000;

	/** random file and SQL script are located here */
	private static final String TEMP_FILE_PATH = "src/main/resources/";

	public static void main(String[] args) throws SQLServerException, ClassNotFoundException, SQLException, IOException {
		executeInsert();
	}
	
	private static void executeInsert() throws ClassNotFoundException, IOException, SQLException {
		// read and split file
		String fileInString = readLineByLineJava8("src/main/resources/fo_random.txt");
		String[] lines = fileInString.split(System.getProperty("line.separator"));

		boolean first = true;
		int batchCounter = 1;
		int lineCounter = 1;
		long timeInNanos = System.nanoTime();
		
		String filePath = TEMP_FILE_PATH + "temp_" + timeInNanos + ".txt";
		StringBuilder sb = new StringBuilder();

		// for loop imitates steady data stream
		for (String line : lines) {
			try {

				// skip first line with headers
				if (first) {
					first = false;
					continue;
				}

				// when cache is full (BATCH_SIZE), flush to database
				if (batchCounter == BATCH_SIZE) {
					sb.append(System.nanoTime() + "|" + line);
					insertToDB(sb, timeInNanos, filePath);
					deleteTempFile(filePath);

					batchCounter = 0;
					lineCounter++;
					timeInNanos = System.nanoTime();
					filePath = TEMP_FILE_PATH + "temp_" + timeInNanos + ".txt";

					// reset StringBuilder
					sb = new StringBuilder();
				} else {
					sb.append(System.nanoTime() + "|" + line + "\r\n");
					lineCounter++;
					batchCounter++;
				}

				// TODO implement any logic here

				// end of loop - this will not happen in real use case
				if (lineCounter == lines.length) {
					sb.append(System.nanoTime() + "|" + line);
				}
			} catch (Exception e) {
				insertToDB(sb, timeInNanos, filePath);
				deleteTempFile(filePath);
				batchCounter = 0;
				lineCounter++;
				e.printStackTrace();
			}
		}

		// insert rest of sdata - this will not happen in real use case
		insertToDB(sb, timeInNanos, filePath);
		deleteTempFile(filePath);

		System.out.println("Remainder of records inserted: " + (batchCounter));
	}
	
	private static String readLineByLineJava8(String filePath) throws IOException {
		return new String(Files.readAllBytes(Paths.get(filePath)), StandardCharsets.UTF_8);
	}

	/** delete temporary file */
	private static void deleteTempFile(String filePath) {
		File fileToDelete = new File(filePath);
		if (fileToDelete.exists()) {
			fileToDelete.delete();
		}
	}

	/** write data to file and insert to database */
	private static void insertToDB(StringBuilder sb, long timeInNanos, String filePath) throws IOException, ClassNotFoundException, SQLException {
		writeStringToTempFile(sb.toString(), timeInNanos, filePath);
		batchInsert(filePath);
	}

	/** write data to file for later bulk insert */
	private static void writeStringToTempFile(String line, long timeInMili, String filePath) throws IOException {

		File f = new File(filePath);
		f.getAbsoluteFile();
		if (!f.exists()) {
			f.createNewFile();
		}

		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filePath, true)));) {
			writer.println(line);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/** batch insert cached data */
	private static void batchInsert(String fileName) throws ClassNotFoundException, SQLException {

		long startTime = System.currentTimeMillis();
		SQLServerBulkCSVFileRecord fileRecord = null;
		fileRecord = new SQLServerBulkCSVFileRecord(fileName, null, "\\|", false);
		fileRecord.addColumnMetadata(1, null, java.sql.Types.NVARCHAR, 50, 0);
		fileRecord.addColumnMetadata(2, null, java.sql.Types.NVARCHAR, 50, 0);
		fileRecord.addColumnMetadata(3, null, java.sql.Types.INTEGER, 0, 0);
		fileRecord.addColumnMetadata(4, null, java.sql.Types.NVARCHAR, 0, 0);
		fileRecord.addColumnMetadata(5, null, java.sql.Types.NVARCHAR, 0, 0);
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection destinationConnection = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=paurus", "paurusUser", "Test1234");
		SQLServerBulkCopyOptions copyOptions = new SQLServerBulkCopyOptions();

		copyOptions.setBatchSize(300000);
		copyOptions.setTableLock(true);

		SQLServerBulkCopy bulkCopy = new SQLServerBulkCopy(destinationConnection);
		bulkCopy.setBulkCopyOptions(copyOptions);
		bulkCopy.setDestinationTableName(TABLE_NAME);
		bulkCopy.writeToServer(fileRecord);

		fileRecord.close();
		bulkCopy.close();
		destinationConnection.close();

		System.out.println("Bulk insert took [" + (System.currentTimeMillis() - startTime) + "] ms");

	}
}
