USE [paurus]
GO

/****** Object:  Table [dbo].[TestTable2]    Script Date: 06/09/2021 20:04:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PerformanceTable] (
	[date_insert] [varchar](50),
	[matchID] [varchar](50),
	[marketID] [int],
	[outcomeID] [varchar](50) NULL,
	[specifiers] [varchar](50) NULL
) ON [PRIMARY]
GO


